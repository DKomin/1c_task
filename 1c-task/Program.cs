﻿using System;

namespace IslandSearch
{
	class Program
	{

		public static int countIslands(int[,] cells, int m, int n)
		{
			if (cells == null || m == 0) return 0;
			int result = 0;
			for (int i = 0; i < m; i++)
			{
				for (int j = 0; j < n; j++)
				{
					if (cells[i, j] == 1)
					{
						result++;
						doFill(cells, i, j);
					}
				}
			}
			return result;
		}
		public static void doFill(int[,] cells, int row, int col)
		{
			if (row < 0 || col < 0 || row >= cells.GetLength(0) || col >= cells.GetLength(1) || cells[row, col] == 0)
			{
				return;
			}
			cells[row, col] = 0;
			int[] dr = new[] { -1, 0, 1, 0 };
			int[] dc = new[] { 0, 1, 0, -1 };
			for (int i = 0; i < dr.Length; i++)
			{
				doFill(cells, row + dr[i], col + dc[i]);
			}
		}

		static void Main(string[] args)
		{
			int m = Convert.ToInt32(Console.ReadLine());
			int n = Convert.ToInt32(Console.ReadLine());

			int[,] cells = new int[m, n];
			Random rng = new Random();
			for (int i = 0; i < m; i++)
			{
				for (int j = 0; j < n; j++)
				{
					if (rng.Next(0, 2) == 1)
					{
						cells[i, j] = 1;
						Console.Write('1');
					}
					else
					{
						cells[i, j] = 0;
						Console.Write('0');
					}
				}
				Console.WriteLine();
			}

			Console.WriteLine(countIslands(cells, m, n));
		}
	}
}
